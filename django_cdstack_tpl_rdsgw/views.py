import ipaddress

from django.template import engines

import copy

from django_cdstack_deploy.django_cdstack_deploy.func import generate_config_static
from django_cdstack_tpl_deb_buster.django_cdstack_tpl_deb_buster.views import (
    handle as handle_deb_buster,
)
from django_cdstack_tpl_deb.django_cdstack_tpl_deb.views import handle as handle_deb
from django_cdstack_tpl_deb.django_cdstack_tpl_deb.views import get_os_release
from django_cdstack_tpl_deb_stretch.django_cdstack_tpl_deb_stretch.views import (
    get_apt_packages,
    get_apt_sources,
    get_apt_prefs,
)
from django_cdstack_tpl_crowdsec_main.django_cdstack_tpl_crowdsec_main.views import (
    handle as handle_crowdsec_main,
)
from django_cdstack_tpl_fail2ban.django_cdstack_tpl_fail2ban.views import (
    handle as handle_fail2ban,
)
from django_cdstack_tpl_ipcop.django_cdstack_tpl_ipcop.views import (
    handle as handle_ipcop,
)
from django_cdstack_tpl_wazuh_agent.django_cdstack_tpl_wazuh_agent.views import (
    handle as handle_wazuh_agent,
)
from django_cdstack_tpl_whclient.django_cdstack_tpl_whclient.views import (
    handle as handle_whclient,
)


def handle(zipfile_handler, template_opts, cmdb_host, skip_handle_os=False):
    django_engine = engines["django"]

    module_prefix = "django_cdstack_tpl_rdsgw/django_cdstack_tpl_rdsgw"

    if "apt_packages" not in template_opts:
        template_opts["apt_packages"] = get_apt_packages(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "fail2ban_apps" not in template_opts:
        template_opts["fail2ban_apps"] = copy.deepcopy(template_opts["apt_packages"])

    if "apt_sources" not in template_opts:
        template_opts["apt_sources"] = get_apt_sources(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "apt_preferences" not in template_opts:
        template_opts["apt_preferences"] = get_apt_prefs(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "os_release" not in template_opts:
        template_opts["os_release"] = get_os_release(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "network_route_announce_list" not in template_opts:
        template_opts["network_route_announce_list"] = list()

    template_opts["customer_net_list"] = list()

    for key in template_opts.keys():
        if key.startswith("rds_net_") and key.endswith("_name"):
            key_ident = key[:-5]

            net_var = "network_iface_" + template_opts[key_ident + "_iface"]

            net_cidr4 = ipaddress.ip_network(
                template_opts[net_var + "_ip"]
                + "/"
                + template_opts[net_var + "_netmask"],
                strict=False,
            )

            net_cidr6 = ipaddress.ip_network(
                template_opts[net_var + "_ip6"]
                + "/"
                + template_opts[net_var + "_netmask6"],
                strict=False,
            )

            rds_conf = dict()
            rds_conf["rds_gw_name"] = template_opts[key_ident + "_name"]
            rds_conf["rds_gw_descr"] = template_opts[key_ident + "_descr"]
            rds_conf["rds_gw_iface"] = template_opts[key_ident + "_iface"]
            rds_conf["rds_gw_cidr4"] = str(net_cidr4)
            rds_conf["rds_gw_cidr6"] = str(net_cidr6)

            rds_conf["fw_white_ip_list"] = list()

            for fs_white_ip_key, fs_white_ip_val in template_opts.items():
                if fs_white_ip_key.startswith(key_ident + "_fw_white_ip"):
                    rds_conf["fw_white_ip_list"].append(
                        str(ipaddress.ip_network(fs_white_ip_val, strict=False))
                    )

            template_opts["customer_net_list"].append(rds_conf)

            announce_conf = dict()
            announce_conf["announce_name"] = rds_conf["rds_gw_name"]
            announce_conf["announce_descr"] = rds_conf["rds_gw_descr"]
            announce_conf["announce_iface"] = rds_conf["rds_gw_iface"]
            announce_conf["announce_cidr4"] = rds_conf["rds_gw_cidr4"]
            announce_conf["announce_cidr6"] = rds_conf["rds_gw_cidr6"]

            template_opts["network_route_announce_list"].append(announce_conf)

    rds_cas_servers = list()

    for key in template_opts.keys():
        if key.startswith("rds_cas_server") and key.endswith("_name"):
            key_ident = key[:-5]

            rds_cas_server = dict()
            rds_cas_server["name"] = template_opts[key_ident + "_name"]
            rds_cas_server["ip"] = template_opts[key_ident + "_ip"]

            rds_cas_servers.append(rds_cas_server)

    template_opts["rds_cas_servers"] = rds_cas_servers

    generate_config_static(zipfile_handler, template_opts, module_prefix)
    handle_whclient(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_ipcop(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_fail2ban(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_crowdsec_main(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_wazuh_agent(zipfile_handler, template_opts, cmdb_host, skip_handle_os)

    if not skip_handle_os:
        handle_deb(zipfile_handler, template_opts, cmdb_host, skip_handle_os)

    return True
